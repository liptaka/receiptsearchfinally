package com.receipt.services;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

//import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.receipt.domain.Receipt;

@Service
public class ReceiptService {

	public static Receipt getCurrentReceipt(String name,String countOfReceipt) throws URISyntaxException, JsonProcessingException, IOException{
		
		RestTemplate restTemplate = new RestTemplate();
		String strUri = "http://food2fork.com/api/search?key=dbdb8e888b200cbc30af3bce4f67e98f&q="+name+"&count="+countOfReceipt+"";
		URI uri = new URI(strUri);

		RequestEntity<?> request = RequestEntity.get(uri).accept(MediaType.APPLICATION_JSON).build();
		ResponseEntity<String> response = restTemplate.exchange(request, String.class);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(response.getBody());
		Receipt receipt = mapper.treeToValue(node, Receipt.class);
		
		return receipt;
	}
	
}
