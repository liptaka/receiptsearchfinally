package com.receipt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.receipt.domain.RequestModel;
import com.receipt.adapters.ReceiptAdapter;

@Controller
public class ReceiptController {
	@Autowired
	ReceiptAdapter receiptAdapter;
	
	@RequestMapping(value = "receipt", method = RequestMethod.GET)
	public ModelAndView receiptCont() {

		return new ModelAndView("receipt", "requestModel", new RequestModel());

	}
	@RequestMapping(value = "search", method = RequestMethod.POST)
	public String getNewReceipt(@ModelAttribute("requestModel") RequestModel requestModel, Model model) {

		model.addAttribute("receiptModel", receiptAdapter.retriveReceipt(requestModel));

		return "receipt";
	}
}
