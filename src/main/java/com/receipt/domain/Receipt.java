package com.receipt.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "count",
    "recipes"
})
public class Receipt {

    @JsonProperty("count")
    private Integer count;
    @JsonProperty("recipes")
    private List<Recipe> recipes;
    
    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("recipes")
    public List<Recipe> getRecipes() {
        return recipes;
    }

    @JsonProperty("recipes")
    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

}  