package com.receipt.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "title",
    "source_url",
    "recipe_id",
    "image_url"
})
@JsonIgnoreProperties({
	"publisher",
	"f2f_url",
	"social_rank",
	"publisher_url"
})
public class Recipe {

    @JsonProperty("title")
    private String title;
    @JsonProperty("source_url")
    private String sourceUrl;
    @JsonProperty("recipe_id")
    private String recipeId;
    @JsonProperty("image_url")
    private String imageUrl;

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("source_url")
    public String getSourceUrl() {
        return sourceUrl;
    }

    @JsonProperty("source_url")
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    @JsonProperty("recipe_id")
    public String getRecipeId() {
        return recipeId;
    }

    @JsonProperty("recipe_id")
    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    @JsonProperty("image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    @JsonProperty("image_url")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}  