package com.receipt.domain;

public class RequestModel {
	
	private String name;
	private String countOfReceipt;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountOfReceipt() {
		return countOfReceipt;
	}
	public void setCountOfReceipt(String countOfReceipt) {
		this.countOfReceipt = countOfReceipt;
	}
}
