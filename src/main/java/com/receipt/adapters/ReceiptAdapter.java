package com.receipt.adapters;

import java.io.IOException;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.receipt.domain.Receipt;
import com.receipt.domain.RequestModel;
import com.receipt.services.ReceiptService;


@Component
public class ReceiptAdapter {
	
	@Autowired
	ReceiptService receiptService;
	
	public Receipt retriveReceipt (RequestModel request){
		Receipt currentReceipt = null;
		
		try {
			currentReceipt = ReceiptService.getCurrentReceipt(request.getName(),request.getCountOfReceipt());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return currentReceipt;
	}
}
