<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<html>
<head>
<title>First spring web-app</title>
<style type="text/css">
body {
	background-color: wheat;
}
</style>

</head>
<body>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v2.9";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

	<h2>Receipt</h2>
	<form:form method="POST" action="search" modelAttribute="requestModel">
		<table>
			<tr>
				<td><form:label path="name">Seeking food name or Food raw material</form:label></td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td><form:label path="countOfReceipt">Count of receipt</form:label></td>
				<td><form:input path="countOfReceipt" /></td>
			</tr>

			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>

	<c:forEach items="${receiptModel.recipes}" var="recipe">
		<!--recipe_id: ${recipe.recipeId}-->
		<p>
			<a href="${recipe.sourceUrl}" target="_blank">${recipe.title}</a><BR>
			<a href="${recipe.sourceUrl}" target="_blank"><img src="${recipe.imageUrl}"></a>
		</p>
		<div class="fb-share-button" data-href="${recipe.sourceUrl}"
			data-layout="button_count" data-size="small"
			data-mobile-iframe="true">
			<a class="fb-xfbml-parse-ignore" target="_blank"
				href="https://www.facebook.com/sharer/sharer.php?u=${recipe.sourceUrl}%2F&amp;src=sdkpreparse">Share</a>
		</div>
	</c:forEach>


</body>
</html>